FROM registry.gitlab.com/admin724/dockerregistry/python:alpine
ENV LANG en_US.UTF-8
ENV TZ   Asia/Tehran
WORKDIR /opt
EXPOSE 80 8080 8000

ARG ENVIRONMENT
ENV ENVIRONMENT                 ${ENVIRONMENT:-development}

ARG DJANGO_SETTINGS_MODULE
ENV DJANGO_SETTINGS_MODULE      ${DJANGO_SETTINGS_MODULE:-core.settings.development}

ARG BROKER_URI
ENV BROKER_URI                  ${BROKER_URI:-pyamqp://guest@rabbitmq//}

ARG DB_NAME
ENV DB_NAME                     ${DB_NAME:-postgres}

ARG DB_USER
ENV DB_USER                     ${DB_USER:-postgres}

ARG DB_PASS
ENV DB_PASS                     ${DB_PASS:-postgres}

ARG DB_HOST
ENV DB_HOST                     ${DB_HOST:-postgres}

ARG DB_PORT
ENV DB_PORT                     ${DB_PORT:-5432}

ARG SECRET_KEY
ENV SECRET_KEY                  ${SECRET_KEY:123}

ARG CELERY_BROKER_URL
ENV CELERY_BROKER_URL           ${CELERY_BROKER_URL:-pyamqp://guest@rabbitmq//}

ARG DSN
ENV DSN                         ${DSN:-dsn}

ARG KAVENEGAR_API
ENV KAVENEGAR_API               ${KAVENEGAR_API:-api}

RUN apk add --no-cache gcc g++ libc-dev libffi-dev jpeg-dev tiff-dev linux-headers libxml2 libxml2-dev libxslt
RUN apk add --no-cache libxml2 libxml2-dev libxslt libxslt-dev gettext-dev gettext
RUN apk add --no-cache postgresql-dev postgresql-client
RUN apk add --no-cache bash curl git tzdata

RUN cp /usr/share/zoneinfo/$TZ /etc/localtime
RUN echo $TZ >  /etc/timezone
RUN echo $TZ >  /etc/TZ

ADD requirements /opt
RUN pip3 install --no-cache-dir --upgrade --force-reinstall --compile pip
RUN pip3 install --no-cache-dir --upgrade --force-reinstall --compile --requirement ${ENVIRONMENT}.txt
ADD src/ /opt

ADD docker-entrypoint.sh /opt
RUN chmod +x /opt/docker-entrypoint.sh

ENTRYPOINT ["/opt/docker-entrypoint.sh"]
