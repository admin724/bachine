from django.utils import timezone
import yaml

CHOICES_HOURS = (
    (0, '0'),
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
    (6, '6'),
    (7, '7'),
    (8, '8'),
    (9, '9'),
    (10, '10'),
    (11, '11'),
    (12, '12'),
    (13, '13'),
    (14, '14'),
    (15, '15'),
    (16, '16'),
    (17, '17'),
    (18, '18'),
    (19, '19'),
    (20, '20'),
    (21, '21'),
    (22, '22'),
    (23, '23'),
)


def upload_path_handler(instance, filename):
    _now = timezone.now().strftime('%Y_%m_%d__%H_%M_%S')
    return "{name}_{id}/{date}_{file}".format(name=instance.backup.device.name,
                                              id=instance.backup.id,
                                              date=_now,
                                              file=filename)


def name_to_uuid(instance, filename):
    """
    convert file.ext to uuid.ext before store in path
    :param instance:
    :param filename:
    :return:
    """
    ext = filename.split('.')[-1]
    if instance.pk:
        return '{}.{}'.format(instance.pk, ext)
    else:
        print('error in name to uuid', instance, filename)
        return


def roster_file_update(roster_file: str, data: dict):
    try:
        with open(roster_file, 'r+') as _file:
            _file_data = yaml.load(_file)
            if not _file_data:
                _file_data = {}
            data.update(_file_data)

        with open(roster_file, 'w+') as _file_out:
            _file_out.write(yaml.dump(data, default_flow_style=False))

        return True

    except Exception as ERROR:
        print("roster_file_update(roster_file: str, data: dict)")
        print(data)
        print(ERROR)
        return False


def roster_file_delete(roster_file: str, device_name):
    try:
        with open(roster_file, 'r+') as _file:
            _file_data = yaml.load(_file)
            if not _file_data:
                _file_data = {}

        if device_name in _file_data:
            del _file_data[device_name]

        with open(roster_file, 'w+') as _file_out:
            _file_out.write(yaml.dump(_file_data, default_flow_style=False))

        return True

    except Exception as ERROR:
        print(device_name)
        print("def roster_file_delete(roster_file: str, device_name):")
        print(ERROR)
        return False
