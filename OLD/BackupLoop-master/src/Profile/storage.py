import os

from django.conf import settings
from django.core.files.storage import FileSystemStorage

BackupStorage = FileSystemStorage(
    location=os.path.join(settings.MEDIA_ROOT, 'Backup'),
    base_url=os.path.join(settings.MEDIA_URL, 'Backup'),
)

ScriptStorage = FileSystemStorage(
    location=os.path.join(settings.MEDIA_ROOT, 'Script'),
    base_url=os.path.join(settings.MEDIA_URL, 'Script'),
)
