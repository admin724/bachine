backup map:

mikrotik > ssh > config
cisco > ssh > config
dslam > ssh > config

windows > minion > file
windows > minion > directory
windows > minion > sql server backup

linux > minion > file
linux > minion > directory
linux > minion > mysql
linux > minion > postgres

linux > ssh > file
linux > ssh > directory
linux > ssh > mysql
linux > ssh > postgres


need runner :
    add host to roster
    remove host from roster
    edit host from roster
    use saltfile --saltfile=/etc/salt/Saltfile :
        raw_shell ( -r , --raw , --raw_shell )
        roster ( --roster )
        roster_file ( --roster-file )
        refresh_cache ( --refresh , --refresh-cache )
        max_procs ( --max-procs )
        extra_filerefs ( --extra-filerefs )
        wipe_ssh ( -w, --wipe )
        ssh_priv ( --priv )
        ignore_host_keys ( -i , --ignore-host-keys )
        ssh_user ( --user )
        ssh_passwd ( --passwd )
        ssh_askpass ( --askpass )
        ssh_key_deploy ( --key-deploy )
        ssh_scan_ports ( --scan-ports )
        ssh_scan_timeout ( --scan-timeout )


    add minions
    remove minions

