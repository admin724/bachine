"""
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

"""
from .base import *

DEBUG = False
ALLOWED_HOSTS = ['1.1.1.1']
SECRET_KEY = os.environ.get('SECRET_KEY', 'og#0021f&cohh66os5pep_lt&9-c%m(2)*f(pikn3@j#hx%we8')
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
DATABASES = {'default':
                 {'ENGINE': 'django.db.backends.postgresql',
                  'NAME': os.environ.get('DB_NAME', 'postgres'),
                  'USER': os.environ.get('DB_USER', 'postgres'),
                  'PASSWORD': os.environ.get('DB_PASS', 'postgres'),
                  'HOST': os.environ.get('DB_HOST', 'localhost'),
                  'PORT': os.environ.get('DB_PORT', '5432'),
                  'ATOMIC_REQUESTS': True,
                  'CONN_MAX_AGE': 60,
                  'AUTOCOMMIT': True,
                  'OPTIONS': {},
                  'TIME_ZONE': None,
                  'TEST': {'CHARSET': None, 'COLLATION': None, 'NAME': None, 'MIRROR': None}}
             }
