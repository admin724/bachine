"""
https://medium.com/@yehandjoe/celery-4-periodic-task-in-django-9f6b5a8c21c7
"""

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = ['*', '127.0.0.1']
SECRET_KEY = os.environ.get('SECRET_KEY', 'cohh66os5pep_lt&9-c%m(2)*f(pikn3@j#hxog#0021f&%we8')

#  add debug_toolbar
DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]

DEBUG_TOOLBAR_CONFIG = {
}
INSTALLED_APPS.append('debug_toolbar')
MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')

# corsheaders
INSTALLED_APPS.append('corsheaders')
MIDDLEWARE.append('corsheaders.middleware.CorsMiddleware')
MIDDLEWARE.append('django.middleware.common.CommonMiddleware')
CORS_ORIGIN_ALLOW_ALL = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {'default':
                 {'ENGINE': 'django.db.backends.postgresql',
                  'NAME': os.environ.get('DB_NAME', 'postgres'),
                  'USER': os.environ.get('DB_USER', 'postgres'),
                  'PASSWORD': os.environ.get('DB_PASS', 'postgres'),
                  'HOST': os.environ.get('DB_HOST', 'localhost'),
                  'PORT': os.environ.get('DB_PORT', '5432'),
                  'ATOMIC_REQUESTS': True,
                  'CONN_MAX_AGE': 60,
                  'AUTOCOMMIT': True,
                  'OPTIONS': {},
                  'TIME_ZONE': None,
                  'TEST': {'CHARSET': None, 'COLLATION': None, 'NAME': None, 'MIRROR': None}}
             }
